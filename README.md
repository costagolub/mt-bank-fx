# Markup for Mtb Forex Site

## First setup

Just run `bower i` and `npm i` inside project root.

## Basic cli commands to develop 

- Start development server which allows you to work under src file at `./app` directory.
```bash
./node_modules/.bin/gulp serve 
```

- Build project. After that command you get prepared files at `./dist` directory. 
This command will worried by itself about clean prev build and creating of the new.
```bash 
./node_modules/.bin/gulp 
```

### Minor settings 
- There are 2 folders to build up the website pages and iframe itself. To switch between different paths need to change gulp variables manually (might be changed fruther to environment varible to pass via the terminal). 

Inside the ```bash ./gulpfile.js``` there are persistant variables: 'target' and 'dist'. 
To build the main app pages have to set the next values up to 'app' and 'dist' respectively.
_______

To build the iframe form page change it to the 'form' and 'dist_iframe'. 
These 2 path are work independently and has their own styles/scripts/media files. 

